const csvtojson = require('csvtojson');
const fs = require('fs');

const filePathMatches = `${__dirname}/../data/matches.csv`;

csvtojson().fromFile(filePathMatches).then( (json) => {
    console.log(json);

    fs.writeFileSync('matches.json', JSON.stringify(json), 'utf-8', (error) => {

        if(error) console.log(error);
        
    } )
    
    
} )

const filePathDeliveries = `${__dirname}/../data/deliveries.csv`;

csvtojson().fromFile(filePathDeliveries).then((json) =>{
    console.log(json);

    fs.writeFileSync('deliveries.json', JSON.stringify(json), 'utf-8', (error) => {
        if(error) console.log(error);
    } )
    
})