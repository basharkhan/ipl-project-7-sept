// 3. Test case for 3rd question

const extraRun = require('./index');
console.log(extraRun);

test("expecting 1 team to concede extra runs below 100 in year 2016", () => {

    let teams = 0;
    for(let key in extraRun){

        if(extraRun[key] < 100 ) teams++;
    }
    expect(teams).toBeGreaterThanOrEqual(1);
    
})
