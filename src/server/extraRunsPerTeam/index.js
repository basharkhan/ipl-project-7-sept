const matches = require("../../data/matches.json");
const deliveries = require("../../data/deliveries.json");

// 3. Extra runs conceded per team in the year 2016

function ipl3(matches, deliveries) {
  if (typeof matches !== "object") {
    throw new Error(`matches is not valid`);
  }

  if (typeof deliveries !== "object") {
    throw new Error(`deliveries is not valid`);
  }

  let idArr = [];

  matches.find((element) => {
    if (element.season == 2016) {
      idArr.push(element.id);
    }
  });

  let extraRuns = deliveries.reduce((acc, curr) => {
    if (idArr.includes(curr.match_id)) {
      if (acc[curr.bowling_team]) {
        acc[curr.bowling_team] =
          acc[curr.bowling_team] + parseInt(curr.extra_runs);
      } else {
        acc[curr.bowling_team] = parseInt(curr.extra_runs);
      }
    }

    return acc;
  }, {});

  return extraRuns;
}

const extraRunsToExport = ipl3(matches, deliveries);

module.exports = extraRunsToExport;
