// 1. Number of matches played per year for all the years in IPL

const matches = require('../../data/matches.json');
const deliveries = require('../../data/deliveries.json');


function ipl1(matches) {

    if(typeof matches !== "object"){
      throw new Error(`matches is not valid`)
    }
    
    const matchesPerYear = matches.reduce((acc, curr) => {
      if (acc[curr.season]) {
        acc[curr.season]++;
      } else {
        acc[curr.season] = 1;
      }
  
      return acc;
    }, {});
  
    return matchesPerYear;
  }
  
  const matchesPerYearToExport = ipl1(matches);

  module.exports = matchesPerYearToExport;
  