// 4. Top 10 economical bowlers in the year 2015
const matches = require('../../data/matches.json');
const deliveries = require('../../data/deliveries.json');

function ipl4(matches, deliveries) {
  if (typeof matches !== "object") {
    throw new Error(`matches is not valid`);
  }

  if (typeof deliveries !== "object") {
    throw new Error(`deliveries is not valid`);
  }

  let idArr = [];

  matches.find((element) => {
    if (element.season == 2015) {
      idArr.push(element.id);
    }
  });

  let totalRunsGivenByBowler = deliveries.reduce((acc, curr) => {
    if (idArr.includes(curr.match_id)) {
      if (acc[curr.bowler]) {
        acc[curr.bowler] = acc[curr.bowler] + parseInt(curr.total_runs);
      } else {
        acc[curr.bowler] = parseInt(curr.total_runs);
      }
    }
    return acc;
  }, {});

  for (let key in totalRunsGivenByBowler) {
    totalRunsGivenByBowler[key] = totalRunsGivenByBowler[key] / countOvers(key);
  }

  function countOvers(bowlerName) {
    let balls = 0;
    let wideBalls = 0;
    let noBalls = 0;

    deliveries.forEach((elementId) => {
      // i use here .includes instead of looping on whole idArr array using forEach
      if (idArr.includes(elementId.match_id)) {
        if (elementId.bowler == bowlerName) {
          balls++;
          if (elementId.wide_runs != 0) wideBalls++;
          if (elementId.noball_runs != 0) noBalls++;
        }
      }
    });
    return (balls - wideBalls - noBalls) / 6;
  }

  let sortedEconomy = Object.keys(totalRunsGivenByBowler).sort((a, b) => {
    return totalRunsGivenByBowler[a] - totalRunsGivenByBowler[b];
  });

  let topTen = [];
  for (let index = 0; index < 10; index++) {
    topTen.push(sortedEconomy[index]);
  }

  return topTen;
}

const economicalBowlersToExport = ipl4(matches, deliveries);

module.exports = economicalBowlersToExport;
