const matches = require('../../data/matches.json');
const deliveries = require('../../data/deliveries.json');


// 2. Number of matches won per team per year in IPL.


function ipl2(matches) {

    if(typeof matches !== "object"){
      throw new Error(`matches is not valid`)
    }
  
    return matches.reduce((acc, curr) => {
      
      if(acc[curr.season]){
        if(acc[curr.season][curr.winner]){
          acc[curr.season][curr.winner]++;
        }
        else{
          acc[curr.season][curr.winner] = 1;
        }
      }
  
      else{
        acc[curr.season] = {};
        acc[curr.season][curr.winner] = 1;
      }
      return acc;
    }, {})
    
  }
  
  const teamWonPerYearToExport = ipl2(matches);
  
  module.exports = teamWonPerYearToExport;
  