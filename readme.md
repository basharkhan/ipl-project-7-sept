# IPL PROJECT DOCUMENTATION

## About IPL Project

Statistics related project where we are going to find out some ipl related statistics like number of matches played or best economical bowlers etc.

---

## Files Provided

- **matches.csv** --> Contains attributes (columns) like id, season, batting team, bowling team, winning team etc.

- **deliveries.csv** --> Contains attributes like match_id (same as id in matches), bowling team, batting team, extra runs, total runs etc.

---

 ## Instructions related to csv files

 Convert both (matches and deliveries) .csv files into .json files and then use the .json files for accessing the data.

```
const csvtojson = require('csvtojson');
const fs = require('fs');

const filePathMatches = `${__dirname}/../data/matches.csv`;

csvtojson().fromFile(filePathMatches).then( (json) => {
    console.log(json);

    fs.writeFileSync('matches.json', JSON.stringify(json), 'utf-8', (error) => {

        if(error) console.log(error);

    } )

} )

```

Above code is for conversion of .csv to .json which uses csvtojson library.

---

 ## Project Prerequisites

Create folder for every question, for example - matchesPerSeason is the folder and all files related to first question goes into this folder.
Hence we have **index.js** and **index.test.js** inside the matchesPerSeason folder.

In **index.js**, we write function of the problem and inside **index.test.js** we write test using jest.

[For info about jest, Click here](https://jestjs.io/docs/getting-started)

Same Pattern to be followed for all the questions.

---

Let's proceed to approach for solving all questions.

 ## 1. Number of matches played per year for all the years in IPL

Before proceeding to explanation, let's see how the small sample of output would look like -

```
{
    2008: 58,
    2009: 57,
    ...
    2017: 59
}

```

Here we use .reduce().

[For info about .reduce(), Click here](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/Reduce)

we only use matches.json here.

we use .reduce() to iterate over each element in matches.json and then we check if , for example - "2008" is available in output object, if no, then we insert "2008" as key in the output object and give value = 1 to that "2008" because it appears for first time.

If yes, then we just increment it with 1.

```
const matchesPerYear = matches.reduce((acc, curr) => {
      if (acc[curr.season]) {
        acc[curr.season]++;
      } else {
        acc[curr.season] = 1;
      }

      return acc;
    }, {});

    return matchesPerYear;
  }

```

Now we come to **index.test.js** to write tests.

For writing tests, make sure you have jest installed. 

[For info about jest, Click here](https://jestjs.io/docs/getting-started)

In order to make **index.test.js** to work, we need to do one final thing in **index.js** i.e. export the output (i.e. matchesPerPerson).

```
module.exports = matchesPerPerson;

```
In index.test.js, we use test and expect to test whether our expectation to program is fulfilled.

Suppose we want every year to have atleast 50 matches. So if our expectation met with received value, test will pass otherwise it will fail.

```
const matchesPerYear = require('./index');

test("Checking whether everyone crosses the 50 mark", () => {
    
    for(let key in matchesPerYear){
        expect(matchesPerYear[key]).toBeGreaterThan(50)
    }
    
})

```
---

 ## 2. Number of matches won per team per year in IPL

Since we already saw above about exporting modules, jest etc , so here we directly jump to code.

In this the output looks like this:

```
{
  "2008": {
    "Kolkata Knight Riders": 6,
    "Chennai Super Kings": 9,
    ...
    "Mumbai Indians": 7
  },
  "2009": {
    "Mumbai Indians": 5,
    "Royal Challengers Bangalore": 9,
    ...
    "Kings XI Punjab": 7
  }
  ...
"2017": {
    "Sunrisers Hyderabad": 8,
    "Rising Pune Supergiant": 10,
    ...
    "Gujarat Lions": 4
  }
}

```

So here we again use **.reduce()** with one extra condition that we check if season already there in our output object or not.

If not, we create season (ex- "2008") as key and inside its value we have an object (object inside object) & inside the inner object we have a winning team & we assigned it the value 1, because the winning team appears for 1st time.

If yes, then it means there is already a season inside outer object , then we check whether winning team is there in inner object or not. If not assign team the value = 1, if yes then increment team with 1.

Here is the code for above explanation-

```
return matches.reduce((acc, curr) => {
      
      if(acc[curr.season]){
        if(acc[curr.season][curr.winner]){
          acc[curr.season][curr.winner]++;
        }
        else{
          acc[curr.season][curr.winner] = 1;
        }
      }
  
      else{
        acc[curr.season] = {};
        acc[curr.season][curr.winner] = 1;
      }
      return acc;
    }, {})

```

Below is index.test.js code - 

```
const teamWonPerYear = require('./index');

test("Expecting that kkr won upto 6 matches in 2008", () => {

expect(teamWonPerYear["2008"]["Kolkata Knight Riders"]).toBeLessThanOrEqual(6);
    
})

```

 ## 3. Extra runs conceded per team in the year 2016

Here we have to use both matches.json and deliveries.json because the latter does not contain season.

Output sample -

```
{
  "Rising Pune Supergiants": 108,
  "Mumbai Indians": 102,
  ...
  "Royal Challengers Bangalore": 156
}

```

First here we store id inside an array called idArr so that we have a reference to 2016 inside deliveries.json which does not contain season.

Code for storing all id that belong to 2016 -

```
let idArr = [];

  matches.find((element) => {
    if (element.season == 2016) {
      idArr.push(element.id);
    }
  });

```

Now since we got idArr, we now use **.reduce()** to get extra runs and make it the value for bowling team which is a key in a object we going to create.

In order to match each id in array with id in deliveries, we use **.includes**.

code - 

```
  let extraRuns = deliveries.reduce((acc, curr) => {
    if (idArr.includes(curr.match_id)) {
      if (acc[curr.bowling_team]) {
        acc[curr.bowling_team] =
          acc[curr.bowling_team] + parseInt(curr.extra_runs);
      } else {
        acc[curr.bowling_team] = parseInt(curr.extra_runs);
      }
    }

    return acc;
  }, {});

  return extraRuns;
}

```

[For info about .includes(), Click here](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/includes)


Code for test -

```
const extraRun = require('./index');

test("expecting 1 team to concede extra runs below 100 in year 2016", () => {

    let teams = 0;
    for(let key in extraRun){

        if(extraRun[key] < 100 ) teams++;
    }
    expect(teams).toBeGreaterThanOrEqual(1);
    
})

```

---

 ## 4. Top 10 economical bowlers in the year 2015

This also need both .json files.

Output sample should be - 

```
[
  "RN ten Doeschate",
  "J Yadav",
  "V Kohli",
  ...
  "GB Hogg"
]

```

Like above question, here we also have to get an aray of id called idArr.

In this problem, two things should need to be done, first, we calculate total runs given by particular bowler for every bowler using **.reduce()**.

Below is the code.

```
let totalRunsGivenByBowler = deliveries.reduce((acc, curr) => {
    if (idArr.includes(curr.match_id)) {
      if (acc[curr.bowler]) {
        acc[curr.bowler] = acc[curr.bowler] + parseInt(curr.total_runs);
      } else {
        acc[curr.bowler] = parseInt(curr.total_runs);
      }
    }
    return acc;
  }, {});

```

Second thing is, we calculate total overs bowler bowled in order to complete the formula i.e.

Economy = Total Runs / Total Overs.

Now for total overs, we need to iterate over object which has bowler as key and total runs as value. We iterate using **for in** loop and then in value of every bowler we divide that value with a function which calculate total overs.

Here is the code -

```
for (let key in totalRunsGivenByBowler) {
    totalRunsGivenByBowler[key] = totalRunsGivenByBowler[key] / countOvers(key);
  }

  function countOvers(bowlerName) {
    let balls = 0;
    let wideBalls = 0;
    let noBalls = 0;

    deliveries.forEach((elementId) => {
      // i use here .includes instead of looping on whole idArr array using forEach
      if (idArr.includes(elementId.match_id)) {
        if (elementId.bowler == bowlerName) {
          balls++;
          if (elementId.wide_runs != 0) wideBalls++;
          if (elementId.noball_runs != 0) noBalls++;
        }
      }
    });
    return (balls - wideBalls - noBalls) / 6;
  }


```
Now after getting an object which has economy as value and bowler as key, we sort then and pick the top ten bowlers.

code for sorting -

```
let sortedEconomy = Object.keys(totalRunsGivenByBowler).sort((a, b) => {
    return totalRunsGivenByBowler[a] - totalRunsGivenByBowler[b];
  });

```

code for picking top ten -

```
  let topTen = [];
  for (let index = 0; index < 10; index++) {
    topTen.push(sortedEconomy[index]);
  }

```

code for tests -

```
const economy = require('./index')

test("Expecting MC Henriques in top ten bowlers with best economy in 2015", () => {

expect(economy[6]).toBe("MC Henriques")
        
})

```

---
 # THE END


